import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_thingspeak/flutter_thingspeak.dart';
import 'model_class/thing_speak_model_class.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController count = TextEditingController();
  bool showTemperature = false;

  ThingSpeakData? thingSpeakData; // Declare a variable to hold fetched data

  @override
  void initState() {
    super.initState();
    fetchData(); // Fetch data when the widget initializes
  }

  Future<void> fetchData() async {
    final flutterThingspeak = FlutterThingspeakClient(channelID: '2524566',readApiKey: "XAIAAYSTYCH26TC6");

    // Initialize the client
    await flutterThingspeak.initialize();

    try {
      // Get data from the ThingSpeak channel
      final result = await flutterThingspeak.getAllData();
      log(result.toString());

      // Parse the JSON data using the model class
      setState(() {
        thingSpeakData = ThingSpeakData.fromJson(result);
        // log(thingSpeakData.field1);
      });
    } catch (e) {
      // ignore: avoid_print
      print('Failed to fetch data: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cattle Information"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          const SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    showTemperature = !showTemperature;
                  });
                },
                child: Container(
                  height: 200,
                  width: 100,
                  decoration: const BoxDecoration(
                    color: Colors.orangeAccent,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        TextField(
                          controller: count,
                          decoration: const InputDecoration(
                            border: InputBorder.none,
                          ),
                        ),
                       const Text("Cowshed Health"),
                        if (thingSpeakData != null) // Check if data is fetched
                          Text("Field 1: ${thingSpeakData!.field1}"), // Display field1
                        if (thingSpeakData != null)
                          Text("Field 2: ${thingSpeakData!.field2}"),// Display field2
                      ],
                    ),
                  ),
                ),
              ),
              if (showTemperature)
                Container(
                  height: 100,
                  width: 100,
                  decoration: const BoxDecoration(
                    color: Colors.orangeAccent,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: const Center(
                    child: Text("Temperature Field"),
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }
}
