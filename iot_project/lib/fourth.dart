import 'package:flutter/material.dart';

class FourthPage extends StatelessWidget {
  final double humidity;

  const FourthPage( {super.key, required this.humidity});

  @override
  Widget build(BuildContext context) {
    Color circle1Color = Colors.white;
    Color circle1BorderColor = Colors.green;
   
    Color circle3Color = Colors.white;
    Color circle3BorderColor = Colors.red;
    String circle1Message = "";
    String circle2Message = "";
    String circle3Message = "";

    if (humidity < 30) {
      circle1Color = Colors.green;
      circle1Message = "Humidity is normal";
    } else {
      circle3Color = Colors.red;
      circle3Message = "Humidity is too high";
    }

    String combinedMessage = [circle1Message, circle2Message, circle3Message]
        .where((message) => message.isNotEmpty)
        .join("\n");

    return Scaffold(
      appBar: AppBar(
        title: const Text('Cowshed Health',style: TextStyle(color: Colors.white),),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 3, 68, 94),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(20),
          padding: const EdgeInsets.all(20),
          height: 500,
          width: 500,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: const Offset(0, 3),
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Current Humidity Analysis",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
              const SizedBox(height: 70,),
              CircleWidget(
                color: circle1Color,
                borderColor: circle1BorderColor,
              ),
             
              const SizedBox(height: 20),
              CircleWidget(
                color: circle3Color,
                borderColor: circle3BorderColor,
              ),
              const SizedBox(height: 20),
              if (combinedMessage.isNotEmpty)
                Text(
                  combinedMessage,
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 16),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

class CircleWidget extends StatelessWidget {
  final Color color;
  final Color borderColor;

  const CircleWidget({super.key, 
    required this.color,
    required this.borderColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
        border: Border.all(color: borderColor),
      ),
    );
  }
}
