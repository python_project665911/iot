import 'package:flutter/material.dart';
import 'package:iot_project/first.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  double _opacity = 0.0;

  @override
  void initState() {
    super.initState();
    // Start a 3-second timer
    Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        _opacity = 1.0;
      });
    });
    Future.delayed(const Duration(seconds: 3), () {
      // Navigate to the next page after 3 seconds
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => const FirstPage()),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          children: [
            AnimatedOpacity(
              opacity: _opacity,
              duration: const Duration(seconds: 3),
              child: SizedBox(
              
                child: Center(
                  child: Image.network(
                      "https://t4.ftcdn.net/jpg/02/58/94/67/360_F_258946740_dBbJY1rhi3x46WZHgkrbf9OxZ1wJiP8B.jpg"),
                ),
              ),
            ),
            AnimatedOpacity(
              opacity: _opacity,
              duration: const Duration(seconds: 5),
              child: Positioned(
                left: 90,
                top: 350,
                child: GestureDetector(
                  onTap: () {
                    // Navigate to the next page when tapped
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => const FirstPage()),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
