import 'package:flutter/material.dart';
import 'package:iot_project/six.dart';

class PrintAddedInfo extends StatefulWidget {
  final String tagNo;
  final String breed;
  final String age;
  final String weight;
  final String? gender;

  const PrintAddedInfo({
    Key? key,
    required this.tagNo,
    required this.breed,
    required this.age,
    required this.weight,
    this.gender,
  }) : super(key: key);

  @override
  _PrintAddedInfoState createState() => _PrintAddedInfoState();
}

class _PrintAddedInfoState extends State<PrintAddedInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Cattle Health',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 3, 68, 94),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 50,),
            Text('Tag Number: ${widget.tagNo}'),
            Text('Breed: ${widget.breed}'),
            Text('Age: ${widget.age}'),
            Text('Weight: ${widget.weight}'),
            Text('Gender: ${widget.gender ?? "Not provided"}'),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                   

                    // Show popup
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text("Cattle added successfully"),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const CattleInfo()));
                                    },
                                    child: const Text("OK")),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  child: const Text('Done'),
                ),
              ],
            ),
            const SizedBox(height: 50,),
          ],
        ),
      ),
    );
  }
}
