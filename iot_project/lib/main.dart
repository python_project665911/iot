import 'package:flutter/material.dart';
import 'package:flutter_thingspeak/flutter_thingspeak.dart';
import 'package:iot_project/first.dart';
import 'package:iot_project/model_class/getInfo_model_class.dart';
import 'package:iot_project/splash_screen.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import "dart:developer";

dynamic database;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Initialize SQLite database
  database = await openDatabase(
    join(await getDatabasesPath(), 'cattle_database.db'),
    onCreate: (db, version) {
      return db.execute(
        "CREATE TABLE cattle(tagNo INTEGER PRIMARY KEY, breed TEXT, age TEXT, weight TEXT, gender TEXT)",
      );
    },
    version: 1,
  );
  

  // Initialize ThingSpeak client
  final flutterThingspeak = FlutterThingspeakClient(
    channelID: '2524566',
    readApiKey: "XAIAAYSTYCH26TC6",
  );

  // Initialize the client
  await flutterThingspeak.initialize();

  // Get data from the ThingSpeak channel
  final result = await flutterThingspeak.getAllData();
  log("Data: $result");

  // Create instances of Cattle model class
  List<Cattle> cattleList = [];
  if (result is List<Map<String, dynamic>>) {
    for (var data in result) {
      Cattle cattle = Cattle(
        tagNo: data['tagNo'],
        breed: data['breed'],
        age: data['age'],
        weight: data['weight'],
        gender: data['gender'],
      );
      cattleList.add(cattle);
    }
  }

  
  // Print the data
  for (var cattle in cattleList) {
    print('Tag Number: ${cattle.tagNo}');
    print('Breed: ${cattle.breed}');
    print('Age: ${cattle.age}');
    print('Weight: ${cattle.weight}');
    print('Gender: ${cattle.gender ?? "Not provided"}');
    print('------------------');
  }

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}
