class Cattle {
  final String tagNo;
  final String breed;
  final String age;
  final String weight;
  final String? gender;

  Cattle({
    required this.tagNo,
    required this.breed,
    required this.age,
    required this.weight,
    this.gender,
  });
}
