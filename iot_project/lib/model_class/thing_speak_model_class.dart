class ThingSpeakData {
  final String? field1;
  final String? field2;
    final String? field3;

  final String? entryId;
  // Add more fields as needed

  ThingSpeakData({this.field1, this.field2,this.field3,this.entryId});

  factory ThingSpeakData.fromJson(Map<String, dynamic> json) {
    // Access the feeds list from the JSON data
    List<dynamic> feeds = json['feeds'];

    // Access the first entry in the feeds list
    Map<String, dynamic> lastEntry = feeds.last;

    return ThingSpeakData(
      field1: lastEntry['field1'].toString(),
      field2: lastEntry['field2'].toString(),
            field3: lastEntry['field3'].toString(),

      entryId: lastEntry['entryId'].toString()
      // Add more fields as needed
    );
  }

  // If you want to add more custom methods or properties, you can do so here
}
