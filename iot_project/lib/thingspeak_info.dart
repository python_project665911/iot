import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_thingspeak/flutter_thingspeak.dart';
import 'package:iot_project/model_class/thing_speak_model_class.dart';

class ThingSpeakInfo extends StatefulWidget {
  final String tagNo;
  final String name;

  const ThingSpeakInfo({
    super.key,
    required this.tagNo,
    required this.name,
  });

  @override
  State<ThingSpeakInfo> createState() => _ThingSpeakInfoState();
}

class _ThingSpeakInfoState extends State<ThingSpeakInfo> {
  bool showTemperature = false;

  ThingSpeakData? thingSpeakData; // Declare a variable to hold fetched data

  @override
  void initState() {
    super.initState();
    fetchData(); // Fetch data when the widget initializes
  }

  Future<void> fetchData() async {
    final flutterThingspeak = FlutterThingspeakClient(
        channelID: '2524566', readApiKey: "XAIAAYSTYCH26TC6");

    // Initialize the client
    await flutterThingspeak.initialize();

    try {
      // Get data from the ThingSpeak channel
      final result = await flutterThingspeak.getAllData();
      log(result.toString());

      // Parse the JSON data using the model class
      setState(() {
        thingSpeakData = ThingSpeakData.fromJson(result);
        // log(thingSpeakData.field1);
      });
    } catch (e) {
      // ignore: avoid_print
      print('Failed to fetch data: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Cattle Info',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 3, 68, 94),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
      ),
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(20),
          padding: const EdgeInsets.all(20),
          height:400,
          width: 500,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: const Offset(0, 3),
              ),
            ],
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 100,
                  width: 100,
                  child: Image.asset("assets/images/cow_shelter.png"),
                ),
                const SizedBox(height: 20),
                const SizedBox(height: 10),
                SizedBox(
                  height: 233,
                  width: 300,
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      Text('Tag Number: ${widget.tagNo}'),
                      Text('Breed Name: ${widget.name}'),
                      const SizedBox(height: 25),
                      Container(
                        height: 120,
                        width: 300,
                        decoration: const BoxDecoration(
                            //color: Color.fromARGB(255, 165, 131, 131)
                            ),
                        child: Column(
                          children: [
                            const Text(
                              'Cattle Temperature',
                              style: TextStyle(fontSize: 20),
                            ),
                            const SizedBox(height: 10),
                            ElevatedButton(
                                onPressed: () {},
                                child: Column(
                                  children: [
                                    if (thingSpeakData != null)
                                      // Check if data is fetched
                                      Text(
                                          "${thingSpeakData!.field3}"), // Display field1
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
